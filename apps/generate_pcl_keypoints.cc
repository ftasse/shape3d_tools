#include <float.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <random>

// #include <omp.h>
#include <pcl/common/time.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/keypoints/harris_3d.h>
#include <pcl/keypoints/uniform_sampling.h>

#include "utils/cmd_parser.h"
#include "features/normals.h"
#include "io/off_io.h"
#include "utils/pcl_utils.h"

float compute_mesh_resolution(pcl::PolygonMesh& mesh, const pcl::PointCloud<pcl::PointXYZ>::Ptr points) {
  std::vector<float> edge_lengths;

  for (size_t f = 0; f < mesh.polygons.size(); ++f) {
    int valence = mesh.polygons[f].vertices.size();
    for (size_t i  = 0; i < valence; ++i) {
      Eigen::Vector3f v1 = points->points[mesh.polygons[f].vertices[i]].getVector3fMap();
      Eigen::Vector3f v2 = points->points[mesh.polygons[f].vertices[(i+1)%valence]].getVector3fMap();
      if (mesh.polygons[f].vertices[i] > mesh.polygons[f].vertices[(i+1)%valence]) {
        edge_lengths.push_back((v1-v2).norm());
      }
    }
  }
  std::sort(edge_lengths.begin(), edge_lengths.end());
  printf("edges count: %d\n", edge_lengths.size());

  // if (edge_lengths.size()%2 == 1) return edge_lengths[edge_lengths.size()/2];
  // else return (edge_lengths[edge_lengths.size()/2]+edge_lengths[1+edge_lengths.size()/2])/2.0;

  float sum = 0;
  for (size_t i = 0; i < edge_lengths.size(); ++i)  sum += edge_lengths[i]/edge_lengths.size();
  return sum;
}

void generate_keypoints(
  pcl::PolygonMesh& mesh, pcl::PointCloud<pcl::PointXYZ>::Ptr sampled_points, std::vector<uint>& indices, 
  std::string ktype, float mr_multiplier, float neighbourhood_radius_percent, bool use_topology, bool verbose) 
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr points(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
  pcl::fromPCLPointCloud2(mesh.cloud, *points);

  float support_radius = neighbourhood_radius_percent/getNormalizingScaleFactor(points, false);
  float mr = 0.01*support_radius; // mesh_resolution
  float scale = support_radius;
  if (mesh.polygons.size() > 0) {
    mr = compute_mesh_resolution(mesh, points);
    scale = mr_multiplier*mr;
  }

  if (verbose) printf("computed support radius and scale: %f %f\n", support_radius, scale);

  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud(points);

  computeNormalsIfNecessary(mesh, points, normals, support_radius, tree, use_topology, true);

  if (ktype == "iss") {
    pcl::ISSKeypoint3D<pcl::PointXYZ, pcl::PointXYZ> iss_detector;
    iss_detector.setSearchMethod (tree);
    iss_detector.setSalientRadius (scale);
    iss_detector.setNonMaxRadius (4.0*mr);

    // iss_detector.setNormalRadius (4.0/6.0 * support_radius);
    // iss_detector.setBorderRadius (4.0/6.0 * support_radius);
    // iss_detector.setAngleThreshold (static_cast<float> (M_PI) / 3.0);

    iss_detector.setThreshold21 (0.975);
    iss_detector.setThreshold32 (0.975);
    iss_detector.setMinNeighbors (5);
    iss_detector.setNumberOfThreads (1);
    iss_detector.setInputCloud (points);
    iss_detector.setNormals(normals);
    iss_detector.compute (*sampled_points);
  } else if (ktype == "uniform") {
    pcl::UniformSampling<pcl::PointXYZ> uniform_sampling;
    pcl::PointCloud<int> keypointIndices;
    uniform_sampling.setInputCloud (points);
    uniform_sampling.setRadiusSearch (scale);
    uniform_sampling.compute(keypointIndices);
    pcl::copyPointCloud(*points, keypointIndices.points, *sampled_points);
  } else if (ktype == "harris") {
    pcl::HarrisKeypoint3D<pcl::PointXYZ,pcl::PointXYZI>::Ptr harris3D(
      new pcl::HarrisKeypoint3D<pcl::PointXYZ,pcl::PointXYZI> (pcl::HarrisKeypoint3D<pcl::PointXYZ,pcl::PointXYZI>::HARRIS));
    pcl::PointCloud<pcl::PointXYZI>::Ptr harris_points(new pcl::PointCloud<pcl::PointXYZI>);

    harris3D->setNonMaxSupression (true);
    harris3D->setRadius (scale);
    harris3D->setRadiusSearch (scale);

    harris3D->setSearchMethod (tree);
    harris3D->setInputCloud (points);
    harris3D->setNormals(normals);
    harris3D->compute (*harris_points);
    sampled_points->width = harris_points->width;
    sampled_points->height = harris_points->height;
    sampled_points->points.resize(harris_points->size());
    for (size_t i = 0; i < harris_points->size(); ++i) {
      sampled_points->points[i].x = harris_points->points[i].x;
      sampled_points->points[i].y = harris_points->points[i].y;
      sampled_points->points[i].z = harris_points->points[i].z;
    }
    // pcl::fromPCLPointCloud2(harris_points, *sampled_points);
  }
  else {
    fprintf(stderr, "Could not find implementation for keypoint detector: %s.\n", ktype.c_str());
  }

  if (verbose) printf("Finished detecting %d keypoints.\n", sampled_points->size());

  indices.clear();
  indices.resize(sampled_points->size());
  for (size_t i = 0; i < indices.size(); ++i) {
    std::vector<int> nearest;
    std::vector<float> nearest_dist;
    if (tree->nearestKSearch(sampled_points->points[i], 1, nearest, nearest_dist) > 0) {
      indices[i] = nearest.front();
    }
  }
}

bool saveOFFFile(const std::string& filename, const pcl::PointCloud<pcl::PointXYZ>& sampled_points) {
  std::ofstream ofs(filename.c_str());
  if (!ofs.is_open()) return false;

  ofs << "OFF\n";
  ofs << sampled_points.size() << " 0 0\n";

  for (size_t i=0; i < sampled_points.size(); ++i) {
    pcl::PointXYZ point = sampled_points.points[i];
    ofs << point.x << " " << point.y << " " << point.z << "\n";
  }

  ofs.close();
  return true;
}

bool savePIDFile(const std::string& filename, const std::vector<uint>& indices) {
  std::ofstream ofs(filename.c_str());
  if (!ofs.is_open()) return false;
  for (size_t i = 0; i < indices.size(); ++i) ofs << indices[i] << "\n";
  ofs.close();
  return true;
}


int main (int argc, char** argv) {
  // Parse cmd flags
  std::map<std::string, std::string> flags;
  parseCmdArgs(argc, argv, &flags);
  bool arg_error = false;

  std::string model_filename, model_filelist;
  std::string output_filelist, output_prefix;
  std::string ktype;
  float radius_percent, mr_multiplier;
  bool verbose, use_topology;
  if (!(getFlag(flags, "model", "",  &model_filename) ^
        getFlag(flags, "model_filelist", "",  &model_filelist))) {
    printf("Error: specify --model xor --model_filelist\n");
    arg_error = true;
  }
  if (model_filelist.size() > 0 &&
      !getFlag(flags, "output_filelist", "",  &output_filelist)) {
    printf("Error: specify --output_filelist\n");
    arg_error = true;
  }
  if (!getFlag(flags, "output_prefix", "",  &output_prefix)) {
    printf("Error: specify --output_prefix\n");
    arg_error = true;
  }
  getFlag(flags, "ktype", "iss",  &ktype);
  getFlag(flags, "radius_percent", 0.1f,  &radius_percent);
  getFlag(flags, "mr_multiplier", 6.0f,  &mr_multiplier);
  getFlag(flags, "use_topology", true,  &use_topology);
  getFlag(flags, "verbose", false,  &verbose);

  std::string output_suffix = "_keypoints.off";
  std::string output_ids_suffix = "_keypoints.pid";

  if (arg_error) return 1;

  if (model_filename.size() > 0) {
    pcl::PolygonMesh mesh;
    if (!pcl::io::loadPolygonFile(model_filename, mesh)) {
      printf("Error: could not load model from %s.\n", model_filename.c_str());
      return 1;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr sampled_points (new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<uint> indices;
    generate_keypoints(mesh, sampled_points, indices, ktype, mr_multiplier, radius_percent, use_topology, verbose);
    std::string output_filename = 
      generate_output_filename(model_filename, output_suffix, "", output_prefix);
    saveOFFFile(output_filename, *sampled_points);
    savePIDFile(generate_output_filename(model_filename, output_ids_suffix, "", output_prefix), indices);
  }

  if  (model_filelist.size() > 0) {
    std::ifstream model_filelist_in (model_filelist.c_str());
    std::ofstream output_filelist_out (output_filelist.c_str());

    while (!model_filelist_in.eof()) {
      std::string model_id, filename;
      model_filelist_in >> model_id >> filename;
      if (model_id.size() == 0 && filename.size() == 0) continue;

      pcl::PolygonMesh mesh;
      if (!pcl::io::loadPolygonFile(filename, mesh)) {
        printf("Error: could not load model from %s.\n", filename.c_str());
        continue;
      }
      if (verbose) printf("Processing: %s %s %s r=%f\n", model_id.c_str(), filename.c_str(), ktype.c_str(), radius_percent);
      pcl::PointCloud<pcl::PointXYZ>::Ptr sampled_points (new pcl::PointCloud<pcl::PointXYZ>);
      std::vector<uint> indices;
      generate_keypoints(mesh, sampled_points, indices, ktype, mr_multiplier, radius_percent, use_topology, verbose);
      std::string output_filename = 
        generate_output_filename(filename, output_suffix, model_id, output_prefix);
      saveOFFFile(output_filename, *sampled_points);
      savePIDFile(generate_output_filename(filename, output_ids_suffix, model_id, output_prefix), indices);
      output_filelist_out << model_id << " " << output_filename << "\n";
    }

    model_filelist_in.close();
    output_filelist_out.close();
  }
}